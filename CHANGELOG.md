## [1.0.2](https://github.com/draftup/react-use-promise/compare/v1.0.1...v1.0.2) (2019-08-24)

### Bug Fixes

- updates README.md with usage examples ([8501264](https://github.com/draftup/react-use-promise/commit/8501264))

## [1.0.1](https://github.com/draftup/react-use-promise/compare/v1.0.0...v1.0.1) (2019-08-22)

### Bug Fixes

- correct flow types for options argument ([95c3932](https://github.com/draftup/react-use-promise/commit/95c3932))

# 1.0.0 (2019-08-22)

### Features

- usePromise hook ([211133c](https://github.com/draftup/react-use-promise/commit/211133c))
