type State<TValue> = {
  pending: boolean;
  resolved: boolean;
  rejected: boolean;
  value: null | TValue;
  error: null | Error;
};

type Options = {
  errorsToKeep?: { new (): Error }[];
};

type Injector<TValue> = (promise: Promise<TValue>) => void;

export function usePromise<TValue>(
  options?: Options
): [null | State<TValue>, Injector<TValue>];
