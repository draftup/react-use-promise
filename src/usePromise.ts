import * as React from "react";

import { State, Options, Injector } from "..";

export class UsePromiseError extends TypeError {}

const INVALID_EXCEPTION_ERROR_MESSAGE =
  "usePromise accepts only Error-based exceptions.";

const getRawPromiseState = () => ({
  pending: false,
  resolved: false,
  rejected: false,
  value: null,
  error: null
});

export function usePromise<TValue>(
  options: Options
): [null | State<TValue>, Injector<TValue>] {
  const { errorsToKeep = [] } = options || {};

  const promiseRef = React.useRef<null | Promise<TValue>>(null);
  const unmountedRef = React.useRef(false);

  const [promiseState, setPromiseState] = React.useState<null | State<TValue>>(
    null
  );

  if (promiseState && promiseState.error !== null) {
    const { error } = promiseState;

    if (!(error instanceof Error)) {
      throw new UsePromiseError(INVALID_EXCEPTION_ERROR_MESSAGE);
    }

    const keep = errorsToKeep.some(
      ErrorConstructor => promiseState.error instanceof ErrorConstructor
    );

    if (!keep) {
      throw promiseState.error;
    }
  }

  const injector = React.useCallback<Injector<TValue>>(promise => {
    promiseRef.current = promise;

    const isActual = () =>
      unmountedRef.current === false && promise === promiseRef.current;

    const handleResolve = nextValue => {
      if (isActual()) {
        if (nextValue instanceof Error) {
          throw nextValue;
        } else {
          setPromiseState({
            ...getRawPromiseState(),
            resolved: true,
            value: nextValue
          });
        }
      }
    };

    const handleReject = (nextError: Error) => {
      if (isActual()) {
        setPromiseState({
          ...getRawPromiseState(),
          rejected: true,
          error: nextError
        });
      }
    };

    setPromiseState({
      ...getRawPromiseState(),
      pending: true
    });

    promise.then(handleResolve).catch(handleReject);
  }, []);

  React.useEffect(() => {
    return () => {
      unmountedRef.current = true;
    };
  }, []);

  return [promiseState, injector];
}
