# React use promise

Custom React hook to manage promises.

[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)

## Install

```bash
npm i -S @draftup/react-use-promise
```

## Highlights

- Automatic promise cancellation:
  - when componet has been unmounted,
  - when another promise has been injected.
- React way error handling.
- Typescript and Flow type definitions included.

## Usage

### Basic example

`usePromise` is a [custom React hook](https://reactjs.org/docs/hooks-custom.html):

```javascript
const [promiseState, promiseInjector] = usePromise();
```

Upon it's initialization `promiseState` is `null`. Once you inject a promise using `promiseInjector` first time `promiseState` becomes an object with a following signature:

```typescript
type PromiseState<ValueType> = {
  pending: boolean;
  resolved: boolean;
  rejected: boolean;
  value: null | ValueType;
  error: null | Error;
};
```

Here's a simple example of how to use `usePromise` hook in your React component:

```javascript
import { usePromise } from "@draftup/react-use-promise";

const submitService = async () => {
  // Some kind of asynchronous operation goes here.
};

const Component = () => {
  const [promiseState, injectPromise] = usePromise();

  const handleClick = React.useCallback(() => {
    injectPromise(submitService());
  }, [injectPromise]);

  if (promiseState && promiseState.pending) {
    return <span>processing...</span>;
  }

  if (promiseState && promiseState.resolved) {
    return <span>succeeded</span>;
  }

  if (promiseState && promiseState.rejected) {
    return <span>failed</span>;
  }

  return <button onClick={handleClick}>submit</button>;
};
```

[![Edit use-promise-basic](https://codesandbox.io/static/img/play-codesandbox.svg)](https://codesandbox.io/s/use-promise-basic-toi54?fontsize=14)

## Error handling

By default `usePromise` will throw away any instance of `Error` constructor that was either throwed or returned by injected promise. You should take care of this errors by using [error boundaries](https://reactjs.org/docs/error-boundaries.html#introducing-error-boundaries):

```javascript
const submitService = async () => {
  throw new Error("Error message");
};

const Root = () => (
  <ErrorBoundary>
    <Component />
  </ErrorBoundary>
);

const Component = () => {
  const [promiseState, injectPromise] = usePromise();

  const handleClick = React.useCallback(() => {
    injectPromise(submitService());
  }, [injectPromise]);

  // Error instance will be throwed away and will bubble up
  // to the closest error boundary you set.
```

[![Edit use-promise-error-propagation](https://codesandbox.io/static/img/play-codesandbox.svg)](https://codesandbox.io/s/use-promise-basic-wz6nr?fontsize=14)

---

But there are cases when you want to handle some errors locally. To make it possible there are an `errorsToKeep` option you can pass on hook initialization. It takes an array of error constructors whose instances you want to handle locally:

```javascript
const [promiseState, injectPromise] = usePromise({
  errorsToKeep: [MyError]
});
```

With above configuration any instance of `MyError` throwed or returned by promise will end up in a `promiseState.error` property so you can handle it locally:

```javascript
const submitService = async () => {
  throw new MyError("My error message");
};

const Component = () => {
  const [promiseState, injectPromise] = usePromise({
      errorsToKeep: [MyError]
  });

  const handleClick = React.useCallback(() => {
    injectPromise(submitService());
  }, [injectPromise]);

  if (promiseState && promiseState.error) {
    return(
        <span>{promiseState.error.message}</span>
    )
  }

  // ...
```

[![Edit use-promise-error-local](https://codesandbox.io/static/img/play-codesandbox.svg)](https://codesandbox.io/s/use-promise-error-propagation-pt48f?fontsize=14)

---

Take a note that your custom error constructors **must be inherited from native `Error` constructor** if you are going to `throw` them from inside you promise:

```javascript
// This will end up with a custom TypeError throwed away:
// class MyError {}

class MyError extends Error {}

const submitService = async () => {
  throw new MyError("My error message");
};
```

## License

This project is [MIT licensed](./LICENSE).
